# Color picker
![image](screenshot.png)  
Screen color identifier at mouse point, color chooser.
## Features
- show zoomed area around the mouse (zoom is adjustable)
- show colored block filled with a color under the mouse
- show RGB and HEX color indexes of a pixel under the mouse
- show mouse coordinates
- change choosen color by clicking on the color-block
- quit the app by key <Esc> or <Ctrl-Q>.
## Usage
The button with "target" icon, activate main picker function, and show zoomed
area around the mouse.  
After left mouse click, main function stops and save HEX index of the color
to the clipboard.  
Click on the right color block opens the color palette.
