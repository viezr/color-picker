'''
Screen color identifier at mouse point, color chooser.
'''
import tkinter as tk
from tkinter import ttk
from tkinter.colorchooser import askcolor
from PIL import Image, ImageGrab, ImageTk

from style import Style


class ColorPicker(tk.Tk):
    '''
    Color picker root GUI class.
    '''
    def __init__(self):
        self._wtitle = "Color picker"
        tk.Tk.__init__(self, className=self._wtitle)
        self.title(self._wtitle)
        self.attributes("-topmost", True)
        self.style = Style()
        self._colors = self.style.colors
        self.configure(background=self._colors["bg"])
        self.columnconfigure(1, weight=1)
        self.overlay = None
        self.buttons = []

        self.bind("<Control-q>", self.quit)
        self.bind("<Escape>", self.quit)

        self.hex_label_var = tk.StringVar()
        self.rgb_label_var = tk.StringVar()
        self.coord_label_var = tk.StringVar()
        self.zoom_scale_var = tk.DoubleVar()
        self.zoom_label_var = tk.StringVar()

        self.cv_size = 128
        self.labels_width = 16
        self.zoom_fact = 8
        self.zoom_range = (4, 12)

        img_resources = Image.open("img/resources.png")
        capture_image = img_resources.crop((0, 24, 128, 25))
        capture_image = capture_image.resize((self.cv_size, self.cv_size),
            resample=Image.BOX)
        self.capture_image = ImageTk.PhotoImage(image=capture_image)
        picker_image = img_resources.crop((0, 0, 24, 24))
        self.picker_image = ImageTk.PhotoImage(image=picker_image)
        clipboard_image = img_resources.crop((24, 0, 48, 24))
        self.clipboard_image = ImageTk.PhotoImage(image=clipboard_image)
        chooser_image = img_resources.crop((48, 0, 72, 24))
        self.chooser_image = ImageTk.PhotoImage(image=chooser_image)
        quit_image = img_resources.crop((72, 0, 96, 24))
        self.quit_image = ImageTk.PhotoImage(image=quit_image)

        self.main_frame = tk.Frame(self, bg=self._colors["bg"])
        self.main_frame.grid(row=0, column=0, padx=10, pady=10, sticky="WE")

        self.image_canvas = tk.Canvas(self.main_frame, bg="white",
            width=self.cv_size, height=self.cv_size)
        self.image_canvas_container = self.image_canvas.create_image(
            0, 0, image=self.capture_image, anchor="nw")
        self.image_canvas.grid(row=0, column=0)

        self.color_canvas = tk.Canvas(self.main_frame, bg="white",
            width=32, height=self.cv_size, relief="raised")
        self.color_canvas.grid(row=0, column=1, sticky="E")
        self.color_canvas_container = self.color_canvas.create_image(2, 52,
            image=self.chooser_image, anchor="nw")
        self.color_canvas.bind('<Button-1>', self.change_color)

        slider = ttk.Scale(self.main_frame, orient="horizontal",
            from_=self.zoom_range[0], to=self.zoom_range[1],
            variable=self.zoom_scale_var, command=self.set_zoom_scale)
        slider.grid(row=1, column=0, pady=6, sticky="WE")

        hex_label = ttk.Label(self.main_frame,
            textvariable=self.hex_label_var, width=self.labels_width)
        hex_label.grid(row=2, column=0)

        rgb_label = ttk.Label(self.main_frame,
            textvariable=self.rgb_label_var, width=self.labels_width)
        rgb_label.grid(row=3, column=0)

        coordinates = ttk.Label(self.main_frame,
            textvariable=self.coord_label_var, width=self.labels_width)
        coordinates.grid(row=4, column=0)

        self.hex_label_var.set("#ffffff")
        self.rgb_label_var.set("rgb(255, 255, 255)")
        self.coord_label_var.set("x: 0, y: 0")
        self.zoom_scale_var.set(self.zoom_fact)
        self.zoom_label_var.set(f"x{self.zoom_fact}")

        zoom_label = ttk.Label(self.main_frame,
            textvariable=self.zoom_label_var)
        zoom_label.grid(row=1, column=1, sticky="NS")

        hex_cpy_button = ttk.Button(self.main_frame, text="clipboard",
            command=self.hex_clipboard, image=self.clipboard_image)
        hex_cpy_button.grid(row=2, column=1, pady=1)

        rgb_cpy_button = ttk.Button(self.main_frame, text="clipboard",
            command=self.rgb_clipboard, image=self.clipboard_image)
        rgb_cpy_button.grid(row=3, column=1, pady=1)

        get_color_button = ttk.Button(self.main_frame, text="capture",
            command=self.open_overlay, image=self.picker_image)
        get_color_button.grid(row=4, column=1, pady=1)
        self.buttons = (hex_cpy_button, rgb_cpy_button, get_color_button)

    def set_zoom_scale(self, event) -> None:
        '''
        Set zoom scale factor on change.
        '''
        scale = int(float(event))
        self.zoom_fact = scale
        self.zoom_label_var.set(f"x{scale}")

    def change_color(self, _event):
        '''
        Choose color from palette
        '''
        def_color = self.hex_label_var.get()
        color = askcolor(def_color,title="Tkinter Color Chooser")
        if color[0]:
            self.color_canvas.configure(bg = color[1])
            self.hex_label_var.set(color[1])
            self.rgb_label_var.set(f"rgb{color[0]}")

    def open_overlay(self):
        '''
        Overlay fullscreen window for capture pixels color
        '''
        self.overlay = tk.Toplevel(self)
        self.overrideredirect(True)
        self.overlay.title("Choose pixel")
        root_geo = self.winfo_geometry().split("+")
        self.overlay.geometry(root_geo[0])
        self.overlay.configure(background="")
        self.overlay.attributes("-fullscreen", True)
        self.overlay.bind('<Button-1>', self.quit_overlay)
        self.overlay.bind('<Motion>', self.motion)
        self.bind('<Motion>', self.motion)
        self.bind("<Control-q>", self.quit)
        self.bind("<Escape>", self.quit)
        self.overlay.wait_visibility(self.overlay)
        self.switch_buttons()
        self.main_up()

    def switch_buttons(self) -> None:
        '''
        Switch state of buttons after opening overley.
        '''
        state = "disabled" if self.overlay else "normal"
        for button in self.buttons:
            button.configure(state=state)
        if self.overlay:
            self.color_canvas.unbind('<Button-1>')
        else:
            self.color_canvas.bind('<Button-1>', self.change_color)

    def main_up(self):
        '''
        Up main frame above overlay for updating info of mouse moving
        '''
        self.attributes('-topmost', True)
        self.lift()
        self.update()
        self.overlay.after(500, self.main_up)

    def motion(self, event):
        '''
        Capture part of screen (canvas_size / zoom_scale) and get pixel color
        under mouse point. Update information at main window.
        '''
        capture_size = self.cv_size / self.zoom_fact
        x_coord, y_coord = event.x - 1, event.y - 1 # - 1 is a little pointer correction
        crop_x1, crop_y1 = x_coord - capture_size / 2, y_coord - capture_size / 2
        crop_x2, crop_y2 = crop_x1 + capture_size, crop_y1 + capture_size
        img = ImageGrab.grab( bbox=(crop_x1, crop_y1, crop_x2, crop_y2) )
        img = img.resize((self.cv_size, self.cv_size), resample=Image.BOX)
        self.capture_image = ImageTk.PhotoImage(img)
        self.image_canvas.itemconfig(self.image_canvas_container,
            image=self.capture_image)
        self.coord_label_var.set(f"x: {x_coord} - y: {y_coord}")
        rgb = img.getpixel((64, 64))
        self.rgb_label_var.set(f"rgb{rgb}")
        hex_color = ''.join(f"{color:02x}" for color in rgb)
        self.hex_label_var.set(f"#{hex_color}")
        self.color_canvas.configure(bg=f"#{hex_color}")

    def quit_overlay(self, _event):
        '''
        Quit overlay.
        '''
        self.unbind('<Motion>')
        self.hex_clipboard()
        self.overlay.destroy()
        self.overlay = None
        self.switch_buttons()

    def hex_clipboard(self):
        '''
        Copy hex color to clipboard.
        '''
        self.clipboard_clear()
        self.clipboard_append(self.hex_label_var.get())
        self.update()

    def rgb_clipboard(self):
        '''
        Copy rgb color to clipboard.
        '''
        self.clipboard_clear()
        self.clipboard_append(self.rgb_label_var.get())
        self.update()

    def quit(self, _event = None):
        '''
        Quit application.
        '''
        self.destroy()


if __name__ == "__main__":
    app = ColorPicker()
    app.mainloop()
